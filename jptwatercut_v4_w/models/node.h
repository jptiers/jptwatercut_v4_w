#ifndef NODE_H
#define NODE_H

#include <QObject>

class node
{
public:
    node();
    void set_raw_resis(int raw_resis);
    int get_raw_resis();

    void set_raw_capa(int raw_capa);
    int get_raw_capa();

    void set_eng_resis(int eng_resis);
    int get_eng_resis();

    void set_eng_capa(int eng_capa);
    int get_eng_capa();

    void set_date(QString date);
    QString get_date();

    void set_raw_tempe(int raw_temp);
    int get_raw_tempe();

    void set_eng_tempe(float eng_temp);
    float get_eng_tempe();

    //COMPENSATION RESI
    void set_co_res_temp(float co_res_temp);
    float get_co_res_temp();

    void set_co_res_sali(float co_res_sali);
    float get_co_res_sali();

    void set_co_res_flow(float co_res_flow);
    float get_co_res_flow();

    void set_co_res_api(float co_res_api);
    float get_co_res_api();

    //COMPENSATION CAP
    void set_co_cap_temp(float co_cap_temp);
    float get_co_cap_temp();

    void set_co_cap_sali(float co_cap_sali);
    float get_co_cap_sali();

    void set_co_cap_flow(float co_cap_flow);
    float get_co_cap_flow();

    void set_co_cap_api(float co_cap_api);
    float get_co_cap_api();

    //NORMALIZATE

    void set_norm_raw_resis(int norm_res);
    int get_norm_raw_resis();

    void set_norm_raw_capa(int norm_cap);
    int get_norm_raw_capa();


    void init_model(int _raw_resis, int _eng_resis, int _raw_capa, int _eng_capa, QString date, float _co_res_temp, float _co_res_salinidad, float _co_res_flow, float _co_res_api, float _co_cap_temp, float _co_cap_salinidad, float _co_cap_flow, float _co_cap_api, int _raw_normalizate_res, int _raw_normalizate_capa);
    void init_model_temp(int _raw_temp, float _eng_tempe, float _co_res_temp, float _co_res_salinidad, float _co_res_flow, float _co_res_api, float _co_cap_temp, float _co_cap_salinidad, float _co_cap_flow, float _co_cap_api, int _raw_normalizate_res, int _raw_normalizate_capa);

    //Values from table node
    int _raw_resis=0, _eng_resis=0, _raw_capa=0, _eng_capa=0, _raw_temp=0, _raw_normalizate_res=0, _raw_normalizate_capa=0;
    float _eng_tempe=0, _co_res_temp=0, _co_res_salinidad=0, _co_res_flow=0, _co_res_api=0, _co_cap_temp=0, _co_cap_salinidad=0,
    _co_cap_flow=0, _co_cap_api=0;
    QString _date;
private:
};

#endif // NODE_H
