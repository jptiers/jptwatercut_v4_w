#include "node.h"

node::node(){}

void node::set_raw_resis(int raw_resis){
    _raw_resis = raw_resis;
};
int node::get_raw_resis(){
    return _raw_resis;
};

void node::set_raw_capa(int raw_capa){
    _raw_capa = raw_capa;
};
int node::get_raw_capa(){
    return _raw_capa;
};

void node::set_eng_resis(int eng_resis){
    _eng_resis = eng_resis;
};
int node::get_eng_resis(){
    return _eng_resis;
};

void node::set_eng_capa(int eng_capa){
    _eng_capa = eng_capa;
};
int node::get_eng_capa(){
    return _eng_capa;
};

void node::set_date(QString date){
    _date = date;
};
QString node::get_date(){
    return _date;
};

void node::set_raw_tempe(int raw_temp){
    _raw_temp = raw_temp;
};
int node::get_raw_tempe(){
    return _raw_temp;
};

void node::set_eng_tempe(float eng_temp){
    _eng_tempe = eng_temp;
};
float node::get_eng_tempe(){
    return _eng_tempe;
};

//COMPE RESIS
void node::set_co_res_temp(float co_res_temp){
    _co_res_temp = co_res_temp;
};
float node::get_co_res_temp(){
    return _co_res_temp;
};

void node::set_co_res_sali(float co_res_sali){
    _co_res_salinidad = co_res_sali;
};
float node::get_co_res_sali(){
    return _co_res_salinidad;
};

void node::set_co_res_flow(float co_res_flow){
    _co_res_flow = co_res_flow;
};
float node::get_co_res_flow(){
    return _co_res_flow;
};

void node::set_co_res_api(float co_res_api){
    _co_res_api = co_res_api;
};
float node::get_co_res_api(){
    return _co_res_api;
};

//COMPE CAP
void node::set_co_cap_temp(float co_cap_temp){
    _co_cap_temp = co_cap_temp;
};
float node::get_co_cap_temp(){
    return _co_cap_temp;
};

void node::set_co_cap_sali(float co_cap_sali){
    _co_cap_salinidad = co_cap_sali;
};
float node::get_co_cap_sali(){
    return _co_cap_salinidad;
};

void node::set_co_cap_flow(float co_cap_flow){
    _co_cap_flow = co_cap_flow;
};
float node::get_co_cap_flow(){
    return _co_cap_flow;
};

void node::set_co_cap_api(float co_cap_api){
    _co_cap_api = co_cap_api;
};
float node::get_co_cap_api(){
    return _co_cap_api;
};

//NORM DATA
void node::set_norm_raw_resis(int norm_res){
    _raw_normalizate_res = norm_res;
};
int node::get_norm_raw_resis(){
    return _raw_normalizate_res;
};

void node::set_norm_raw_capa(int norm_capa){
    _raw_normalizate_capa = norm_capa;
};
int node::get_norm_raw_capa(){
    return _raw_normalizate_capa;
};

void node::init_model(int raw_resis, int eng_resis, int raw_capa, int eng_capa, QString date,
                      float co_res_temp, float co_res_salinidad, float co_res_flow, float co_res_api,
                      float co_cap_temp, float co_cap_salinidad, float co_cap_flow, float co_cap_api,
                      int raw_normalizate_res, int raw_normalizate_capa){
    this->_raw_resis = raw_resis;
    this->_eng_resis = eng_resis;
    this->_raw_capa = raw_capa;
    this->_eng_capa = eng_capa;
    this->_date = date;
    this->_co_res_temp = co_res_temp;
    this->_co_res_salinidad = co_res_salinidad;
    this->_co_res_flow = co_res_flow;
    this->_co_res_api = co_res_api;
    this->_co_cap_temp = co_cap_temp;
    this->_co_cap_salinidad = co_cap_salinidad;
    this->_co_cap_flow = co_cap_flow;
    this->_co_cap_api = co_cap_api;
    this->_raw_normalizate_res = raw_normalizate_res;
    this->_raw_normalizate_capa = raw_normalizate_capa;
}

void node::init_model_temp(int raw_temp, float eng_tempe, float co_res_temp, float co_res_salinidad,
                           float co_res_flow, float co_res_api, float co_cap_temp, float co_cap_salinidad,
                           float co_cap_flow, float co_cap_api, int raw_normalizate_res, int raw_normalizate_capa){
    this->_raw_temp = raw_temp;
    this->_eng_tempe = eng_tempe;
    this->_co_res_temp = co_res_temp;
    this->_co_res_salinidad = co_res_salinidad;
    this->_co_res_flow = co_res_flow;
    this->_co_res_api = co_res_api;
    this->_co_cap_temp = co_cap_temp;
    this->_co_cap_salinidad = co_cap_salinidad;
    this->_co_cap_flow = co_cap_flow;
    this->_co_cap_api = co_cap_api;
    this->_raw_normalizate_res = raw_normalizate_res;
    this->_raw_normalizate_capa = raw_normalizate_capa;
}
