#ifndef JPTTCPCLIENT_H
#define JPTTCPCLIENT_H

#include <QObject>
#include <QtNetwork>
#include <QtNetwork/QNetworkInterface>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#define def_ip_widgets_communication    "127.0.0.1"
#define def_port_widgets_communication  10000

class jpttcpclient : public QObject{
     Q_OBJECT

public:
    explicit jpttcpclient(QObject *parent = 0);
    void set_json_monitoring(QString json);
    QString get_json_monitoring();

public slots:
    void query_from_server();
    void restart_conection();

    void send_message(QString mode);
    void slot_recieve_eng(QVector<QString> eng,QVector<QString> raw);
    void response_to_monitoring(QString response);

private:
     QTcpSocket *a_client;
     QString json_monitoring;

signals:
     void closed_conection();
     void new_entry(QStringList trame);
     void new_json_entry(QStringList raw_data,QStringList eng_data, QStringList sensors);

};

#endif // JPTTCPCLIENT_H
