#include "jpttcpclient.h"
bool gv_init_comunication = false;

//******************************************************************************************************************************
// Funcion principal                                                                                              [jpttcpclient]
//******************************************************************************************************************************
jpttcpclient::jpttcpclient(QObject *parent) : QObject(parent){
    a_client = new QTcpSocket(this);
    connect(a_client, SIGNAL(readyRead()), this, SLOT(query_from_server()));
    connect(a_client, SIGNAL(disconnected()), this, SLOT(restart_conection()));
    a_client->connectToHost(QHostAddress(def_ip_widgets_communication), def_port_widgets_communication);
}

//******************************************************************************************************************************
// Respuesta proveniente desde el servidor                                                   SIGNAL[QTcpSocket:readyRead - SLOT]
//------------------------------------------------------------------------------------------------------------------------------
void jpttcpclient::query_from_server(){
    QString lv_answer;
    lv_answer.append(a_client->readAll());

    if(lv_answer == "Start")
        gv_init_comunication = true;
    //en la trama vieja el sistema detectaba por nodo la cantidad de caracteres ## y validaba para enviarlos a su respectivo destino
    if(gv_init_comunication)
        if(lv_answer.count("#") > 1){
            QStringList lv_message = lv_answer.split("-");
            lv_message.removeAll("##");
            while(lv_message.size() > 2){
                emit new_entry(lv_message.mid(0,3));
                lv_message = lv_message.mid(3,-1);
            }
        }
    //#endif
    //(Sentencia de visualizacion de trama.
    if(lv_answer!="Start"){
        //seteo el json que envia el monitoring }
        set_json_monitoring(lv_answer);

        //qDebug()<<"LLego Trama nueva en el sistema";
        qDebug()<<"trama server "<<lv_answer;
        //si llega una trama nueva diferente de Start, es decir, de la trama de iniciacion del sistema entonces parceamos la data como json

        QJsonDocument lv_message_json = QJsonDocument::fromJson(lv_answer.toUtf8());//es el documento entrante.
        QJsonObject   lv_trama_json   = lv_message_json.object();//se convierte en objeto.

        //parseamos device object.
        QJsonObject  lv_device=lv_trama_json.value("device").toObject();

        //parseamos respecto a sensores.
        QJsonObject lv_sensors=lv_device.value("sensors").toObject();

        QString temporal_raw[20];
        QString temporal_eng[20];
        QString temporal_sensors[20];

        QStringList parce_list_label=lv_sensors.keys();


        for(int i=0; i<parce_list_label.size();i++)
        {
            //obtenemos valor raw y de eng de cada dato
            QJsonObject sensor_final=lv_sensors.value(parce_list_label[i]).toObject();

            //obtenemos solo la cabecera
            QStringList cabecera=parce_list_label[i].split("-");
            //qDebug()<<"ee "<<parce_list_label[i];
            if (cabecera[0] == "1"){
                //qDebug()<<"Valor 0<<"<<cabecera;
                temporal_raw[0]=(sensor_final.value("RAW").toString());
                temporal_eng[0]=(sensor_final.value("ENG").toString());
                temporal_sensors[0] = (cabecera[1]);

            }else if(cabecera[0] == "2"){
                //qDebug()<<"Valor 1<<"<<cabecera;
                temporal_raw[1]=(sensor_final.value("RAW").toString());
                temporal_eng[1]=(sensor_final.value("ENG").toString());
                temporal_sensors[1] = (cabecera[1]);

            }else if(cabecera[0] == "3"){
                //qDebug()<<"Valor 2<<"<<cabecera;
                temporal_raw[2]=(sensor_final.value("RAW").toString());
                temporal_eng[2]=(sensor_final.value("ENG").toString());
                temporal_sensors[2] = (cabecera[1]);

            }else if(cabecera[0] == "4"){
                //qDebug()<<"Valor 6<<"<<cabecera;
                temporal_raw[6]=(sensor_final.value("RAW").toString());
                temporal_eng[6]=(sensor_final.value("ENG").toString());
                temporal_sensors[6] = (cabecera[1]);
            }else if(cabecera[0] == "5"){
                //qDebug()<<"Valor 7<<"<<cabecera;
                temporal_raw[7]=(sensor_final.value("RAW").toString());
                temporal_eng[7]=(sensor_final.value("ENG").toString());
                temporal_sensors[7] = (cabecera[1]);
            }else if(cabecera[0] == "6"){
                //qDebug()<<"Valor 8<<"<<cabecera;
                temporal_raw[8]=(sensor_final.value("RAW").toString());
                temporal_eng[8]=(sensor_final.value("ENG").toString());
                temporal_sensors[8] = (cabecera[1]);
            }else if(cabecera[0] == "7"){
                //qDebug()<<"Valor 3<<"<<cabecera;
                temporal_raw[3]=(sensor_final.value("RAW").toString());
                temporal_eng[3]=(sensor_final.value("ENG").toString());
                temporal_sensors[3] = (cabecera[1]);
            }else if(cabecera[0] == "8"){
                //qDebug()<<"Valor 4<<"<<cabecera;
                temporal_raw[4]=(sensor_final.value("RAW").toString());
                temporal_eng[4]=(sensor_final.value("ENG").toString());
                temporal_sensors[4] = (cabecera[1]);
            }else if(cabecera[0] == "9"){
                //qDebug()<<"Valor 5<<"<<cabecera;
                temporal_raw[5]=(sensor_final.value("RAW").toString());
                temporal_eng[5]=(sensor_final.value("ENG").toString());
                temporal_sensors[5] = (cabecera[1]);
            }else if(cabecera[0] == "10"){
                //qDebug()<<"Valor 9<<"<<cabecera;
                temporal_raw[9]=(sensor_final.value("RAW").toString());
                temporal_eng[9]=(sensor_final.value("ENG").toString());
                temporal_sensors[9] = (cabecera[1]);
            }else if(cabecera[0] == "11"){
                //qDebug()<<"Valor 10<<"<<cabecera;
                temporal_raw[10]=(sensor_final.value("RAW").toString());
                temporal_eng[10]=(sensor_final.value("ENG").toString());
                temporal_sensors[10] = (cabecera[1]);
            }else if(cabecera[0] == "12"){
                //qDebug()<<"Valor 11<<"<<cabecera;
                temporal_raw[11]=(sensor_final.value("RAW").toString());
                temporal_eng[11]=(sensor_final.value("ENG").toString());
                temporal_sensors[11] = (cabecera[1]);
            }else if(cabecera[0] == "13"){
                //qDebug()<<"Valor 8<<"<<cabecera[0];
                temporal_raw[12]=(sensor_final.value("RAW").toString());
                temporal_eng[12]=(sensor_final.value("ENG").toString());
                temporal_sensors[12] = (cabecera[1]);
            }

        }
        QStringList raw_to_send;
        QStringList eng_to_send;
        QStringList sensors_to_send;

        for(int k=0;k<parce_list_label.size();k++)
        {
            raw_to_send.append(temporal_raw[k]);
            eng_to_send.append(temporal_eng[k]);
            sensors_to_send.append(temporal_sensors[k]);
        }
        //ya organizados los datos procedemos a enviarlos a la clase maestra
        emit new_json_entry(raw_to_send,eng_to_send,sensors_to_send);
    }

}
//******************************************************************************************************************************
// Intenta reiniciar la coneccion con el servidor                                           SIGNAL[QTcpSocket:disconnect - SLOT]
//******************************************************************************************************************************
void jpttcpclient::restart_conection(){
    gv_init_comunication = false;
    a_client->close();
    a_client->connectToHost(QHostAddress(def_ip_widgets_communication), def_port_widgets_communication);
    emit closed_conection();
}

//******************************************************************************************************************************
// Emite el mensaje hacie el servidor                                                    SIGNAL[jptwatercur:send_message - SLOT]
//------------------------------------------------------------------------------------------------------------------------------
void jpttcpclient::send_message(QString mode){

    if((a_client->state() == QAbstractSocket::ConnectedState) && mode == "Start"){
        QJsonObject lv_trame;
        lv_trame["name"]    = "watercut";
        lv_trame["mode"]    = mode;

        QJsonDocument lv_document(lv_trame);
        QString lv_str_trame(lv_document.toJson());

        a_client->write(lv_str_trame.toLatin1().data());

    }else
        restart_conection();

}
//******************************************************************************************************************************
// Emite el mensaje hacia el servidor                                                    SIGNAL[jptwatercut:send_message - SLOT]
//******************************************************************************************************************************


void jpttcpclient::response_to_monitoring(QString response)
{
    if(a_client->state() == QAbstractSocket::ConnectedState){

        QString lv_str_trame(response);//casteo la respuesta

        a_client->write(lv_str_trame.toLatin1().data());

     }else
        restart_conection();

}


//Slot que recibira la data correspondiete para agregar los valors a las cabeceras correspondientes
void jpttcpclient::slot_recieve_eng(QVector<QString> eng, QVector<QString> raw){
    QStringList sensors;
    sensors<<"1-ph"<<"2-dissolved-oxygen"<<"3-tss-180"<<"4-tss-90"<<"5-temperature"<<"6-oil-water"<<"7-conductivity-AC"<<"8-conductivity-DC"<<"9-water-quality-index";
    //es el documento entrante.
    QJsonDocument lv_message_json = QJsonDocument::fromJson(get_json_monitoring().toUtf8());
    //se convierte en objeto.
    QJsonObject   lv_trama_json = lv_message_json.object();
    //parseamos device object.
    QJsonObject  lv_device=lv_trama_json.value("device").toObject();
    //parseamos respecto a sensores.
    QJsonObject lv_sensors=lv_device.value("sensors").toObject();
    //objeto que agregara sensores
    QJsonObject add_sensors = lv_sensors.value("sensors").toObject();

    QString time = lv_device.value("timestamp").toString();

    lv_device.remove("timestamp");
    //lv_device.insert("timestamp", time);
    //cantidad de los sensores
    lv_device.remove("sensor_count");
    //cantidad de los sensores

    for (int i=0;i<sensors.count();i++) {
        //se pone al reves para que salga de primero RAW
        add_sensors.insert("ENG", eng[i]);
        add_sensors.insert("RAW", raw[i]);

        lv_sensors.insert(sensors[i], add_sensors);
    }

    qDebug()<<"======================================";

    //lv_device.remove("sensors");

    lv_trama_json.remove("device");
    lv_device.remove("sensors");
    //
    lv_device.insert("sensor_count","8");
    lv_device.insert("sensors",lv_sensors);
    lv_trama_json.insert("device", lv_device);

    QJsonObject tt = lv_trama_json.value("device").toObject();
    tt.insert("timestamp", time);
    lv_trama_json.remove("device");
    lv_trama_json.insert("device", tt);
    //qDebug()<<lv_trama_json<<" JSON OKAAAA";

    QJsonDocument final_json_document(lv_trama_json);
    QString final_to_send(final_json_document.toJson(QJsonDocument::Compact));
    set_json_monitoring(final_to_send);

    qDebug()<<get_json_monitoring();

    response_to_monitoring(final_to_send);
}

//******************************************************************************************************************************
//SET AND GET JSON FROM JPTMONITORING
//******************************************************************************************************************************
void jpttcpclient::set_json_monitoring(QString json){
    json_monitoring = json;
}

QString jpttcpclient::get_json_monitoring(){
    return json_monitoring;
}
