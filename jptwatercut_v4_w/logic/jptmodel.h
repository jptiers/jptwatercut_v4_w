#ifndef JPTMODEL_H
#define JPTMODEL_H

#include <QObject>
#include <QVector>
#include "math.h"

//-----------------------------------------------------------------------------------------------------------------------------
// Limite de salinidad para aplicar el modelo [Turbulento y laminar; Turbulento]
//-----------------------------------------------------------------------------------------------------------------------------
#define def_lim_salinity            3000

//-----------------------------------------------------------------------------------------------------------------------------
// Tipo de fluido presente en las mezclas
//-----------------------------------------------------------------------------------------------------------------------------
#define def_type_fluid_water        1
#define def_type_fluid_oil          2
#define def_type_fluid_mixture      3

//-----------------------------------------------------------------------------------------------------------------------------
// Cantidad de sensores presentes {resistivos como capacitivos}
//-----------------------------------------------------------------------------------------------------------------------------
#define def_num_sens                6

//-----------------------------------------------------------------------------------------------------------------------------
// Porcentaje maximo y minimo de corte de agua
//-----------------------------------------------------------------------------------------------------------------------------
#define def_value_max_porcentaje    100
#define def_value_min_porcentaje    0

//-----------------------------------------------------------------------------------------------------------------------------
// Limites para seleccionar la curva en los valores capacitivos
//-----------------------------------------------------------------------------------------------------------------------------
#define def_lim_inf_cap_070_100     5400
#define def_lim_sup_cap_070_100     6000
#define def_lim_sup_cap_040_070     7500
#define def_lim_sup_cap_030_040     13500
#define def_lim_sup_cap_000_030     15000

//-----------------------------------------------------------------------------------------------------------------------------
// Posicion del area de trabajo en los diferentes sensores
//-----------------------------------------------------------------------------------------------------------------------------
#define def_res_sensor_area 0
#define def_cap_sensor_area 1

//-----------------------------------------------------------------------------------------------------------------------------
// Ecuacion del sensor Resistivo para obtener el porcentaje de corte en mezclas
// y = e1*raw + e2
//-----------------------------------------------------------------------------------------------------------------------------
#define     def_res_mixture_e1  -0.00925
#define     def_res_mixture_e2  150

//-----------------------------------------------------------------------------------------------------------------------------
// Ecuaciones del sensor de Capacitancia para obtener el porcentaje de corte en mezclas. Revizar PDF Funcion Mezclas "curvas"
//-----------------------------------------------------------------------------------------------------------------------------
#define     def_cap_mixture_e1_70       16755.3
#define     def_cap_mixture_e2_70      -0.000916525

#define     def_cap_mixture_e1_40      -0.00000000400787
#define     def_cap_mixture_e2_40       0.000095837
#define     def_cap_mixture_e3_40      -0.762577
#define     def_cap_mixture_e4_40       2059.96

#define     def_cap_mixture_e1_30      -0.001982076
#define     def_cap_mixture_e2_30       55.50906905

#define     def_cap_mixture_e1_00      -0.00000000804025
#define     def_cap_mixture_e2_00       0.000325028
#define     def_cap_mixture_e3_00      -4.3779
#define     def_cap_mixture_e4_00       19675.2
//-----------------------------------------------------------------------------------------------------------------------------

class jptmodel : public QObject{
    Q_OBJECT

public:
    explicit jptmodel(QObject *parent = nullptr);
    jptmodel(int lim_salinity, QObject *parent);

    void set_lim_salinity(int salinity);
    void aplicate_all_model(QVector<int> res_sensor, QVector<int> cap_sensor, double *lam_cap, double *lam_res, double *tur_cap, double *tur_res, double *var_res, double *var_cap, int lim_lam, QVector<double> *cut_cap, QVector<double> *cut_res);
    void aplicate_high_model(QVector<int> res_sensor, QVector<int> cap_sensor, double *hcut_tur_cap, double *hcut_tur_res, double factor, int x_1);


    void type_model(int salinity, QString &models);

    double select_model_mixture_cap(int raw_cap_sen);
    double select_model_mixture_res(int raw_res_sen);
    double cap_mixture_model_070_100(int raw);
    double cap_mixture_model_040_070(int raw);
    double cap_mixture_model_030_040(int raw);
    double cap_mixture_model_000_030(int raw);
    double res_mixture_model_000_100(int raw);
    double res_laminar(double values_per_corte[6]);
    double cap_laminar(double values_per_corte[6]);
    double res_turbulent(double values_per_corte[6]);
    double cap_turbulent(double values_per_corte[6]);
    double res_turbulent_laminar(double values_per_corte[6]);
    double cap_turbulent_laminar(double values_per_corte[6]);

    //Estos se usan para los valores que se procesan desde la base de datos
    float res_laminar_all(QList<QList<float>>values_per_corte, int min);
    float cap_laminar_all(QList<QList<float>> values_per_corte, int min);
    float res_laminar_EI_all(QList<QList<float>> values_per_corte, int min);
    float cap_laminar_EI_all(QList<QList<float>> values_per_corte, int min);
    float res_turbulent_all(QList<QList<float>> values_per_corte, int min);
    float cap_turbulent_all(QList<QList<float>> values_per_corte, int min);
    float res_lam_turbulent_all(QList<QList<float>> values_per_corte, int min);
    float cap_lam_turbulent_all(QList<QList<float>> values_per_corte, int min);

private:

    QVector<double> a_sen_res;
    QVector<double> a_sen_cap;

    int a_lim_salinity;



};

#endif // JPTMODEL_H
