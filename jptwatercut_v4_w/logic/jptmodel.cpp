﻿#include "jptmodel.h"
#include <QDebug>
//******************************************************************************************************************************
// Constructor                                                                                                        [jptmodel]
//******************************************************************************************************************************
jptmodel::jptmodel(QObject *parent) : QObject(parent){
    a_lim_salinity = def_lim_salinity;
}

//******************************************************************************************************************************
// Constructor con un valor de salinidad definido                                                                     [jptmodel]
//******************************************************************************************************************************
jptmodel::jptmodel(int lim_salinity, QObject *parent) : QObject(parent), a_lim_salinity(lim_salinity){}

//******************************************************************************************************************************
// define los modelos que se van a aplicar segun el valor de salinidad                                                [jptmodel]
//******************************************************************************************************************************
void jptmodel::type_model(int salinity, QString &models){
    models = (salinity < a_lim_salinity) ? "LT" : "LR";
}

//******************************************************************************************************************************
// fija el valor de salinidad                                                                                         [jptmodel]
//******************************************************************************************************************************
void jptmodel::set_lim_salinity(int salinity){
    a_lim_salinity = salinity;
}

//******************************************************************************************************************************
// Aplica la curva de calculo de los capacitivos y resistivos en el modelo turbulento                                 [jptmodel]
//------------------------------------------------------------------------------------------------------------------------------
// Ingresan los valores raw que estan corregidos por temperatura, salinidad y flujo.
//------------------------------------------------------------------------------------------------------------------------------
// cap: {17,16,16,17,16,18}
// res: {17,16,16,17,16,18}
//
const double area_sen[6] = {0.17,0.15,0.15,0.17,0.15,0.21};
//******************************************************************************************************************************
void jptmodel::aplicate_all_model(QVector<int> res_sensor, QVector<int> cap_sensor, double *lam_cap, double *lam_res, double *tur_cap, double *tur_res, double *var_res, double *var_cap, int lim_lam, QVector<double> *cut_cap, QVector<double> *cut_res){

    double med_res(0), med_cap(0);

    for(int f_sens = 0; f_sens < def_num_sens; ++f_sens){

        double pre_res = select_model_mixture_res(res_sensor[f_sens]);
        double pre_cap = select_model_mixture_cap(cap_sensor[f_sens]);

        cut_cap->append(pre_cap);
        cut_res->append(pre_res);

        *tur_res += pre_res;
        *tur_cap += pre_cap;

        pre_cap = pre_cap * area_sen[f_sens];
        pre_res = pre_res * area_sen[f_sens];

        *lam_res += pre_res;
        *lam_cap += pre_cap;


        med_res  += res_sensor[f_sens];
        med_cap  += cap_sensor[f_sens];

    }

    *tur_cap /= def_num_sens;
    *tur_res /= def_num_sens;



    med_res  /= def_num_sens;
    med_cap  /= def_num_sens;


    double lv_var_res(0), lv_var_cap(0);

    for(int f_bsw = 0; f_bsw < def_num_sens; ++f_bsw){
        lv_var_res += pow(res_sensor[f_bsw]-med_res, 2);
        lv_var_cap += pow(cap_sensor[f_bsw]-med_cap, 2);
    }

    *var_cap = sqrt(lv_var_cap / def_num_sens);
    *var_res = sqrt(lv_var_res / def_num_sens);
}


double jptmodel::res_laminar(double values_per_corte[6])
{   //el corte laminar es la sumatoria por areas especificas
    //es decir Corte Laminar=Area1*Corte1+Area2*Corte2+...+Area6*corte6
    //las areas estan definidas en el vector area sensor[6].
    double corte_res_laminar=0;
    for (int j=0;j<6;j++)//funcion que recorrera valores.
    {
        corte_res_laminar=corte_res_laminar+(values_per_corte[j]*area_sen[j]);
    }
    //todos los valores se guardan en la variable corte_res_laminar,
    //es la mism que regresa el sistema con la info
    return corte_res_laminar;

}

//RES LAMINAR PARA TODOOOOOOOO

float jptmodel::res_laminar_all(QList<QList<float>> values_per_corte, int min)
{   //el corte laminar es la sumatoria por areas especificas
    //es decir Corte Laminar=Area1*Corte1+Area2*Corte2+...+Area6*corte6
    //las areas estan definidas en el vector area sensor[6].
    float corte_res_laminar=0;
    for (int i=0;i<min;i++) {
        for (int j=0;j<6;j++)//funcion que recorrera valores.
        {
            //qDebug()<<"wc resistivo "<<values_per_corte[j][i];
            corte_res_laminar=corte_res_laminar+(values_per_corte[i][j]*area_sen[j]);
        }
    }
    //todos los valores se guardan en la variable corte_res_laminar,
    //es la mism que regresa el sistema con la info
    return corte_res_laminar/min;

}

//RES LAMINAR EMULSION INVERSA PARA TODOOOOOOOO
float jptmodel::res_laminar_EI_all(QList<QList<float>> values_per_corte, int min)
{   //el corte laminar es la sumatoria por areas especificas
    //es decir Corte Laminar=Area1*Corte1+Area2*Corte2+...+Area6*corte6
    //las areas estan definidas en el vector area sensor[6].
    float corte_res_laminar=0;
    for (int i=0;i<min;i++) {
        for (int j=0;j<6;j++)//funcion que recorrera valores.
        {
            //qDebug()<<"wc resistivo "<<values_per_corte[j][i];
            corte_res_laminar=corte_res_laminar+(values_per_corte[i][j]*area_sen[j]);
        }
    }
    //todos los valores se guardan en la variable corte_res_laminar,
    //es la mism que regresa el sistema con la info
    return corte_res_laminar/min;

}

//
double jptmodel::cap_laminar(double values_per_corte[6])
{   //el corte laminar es la sumatoria por areas especificas
    //es decir Corte Laminar=Area1*Corte1+Area2*Corte2+...+Area6*corte6
    //las areas estan definidas en el vector area sensor[6].
    double corte_cap_laminar=0;
    for (int j=0;j<6;j++)//funcion que recorrera valores.
    {
        corte_cap_laminar=corte_cap_laminar+(values_per_corte[j]*area_sen[j]);
    }
    //todos los valores se guardan en la variable corte_res_laminar,
    //es la mism que regresa el sistema con la info
    return corte_cap_laminar;

}

//CAP LAMINAR PARA TODOOOOOOOOOOOOOOOOOO
float jptmodel::cap_laminar_all(QList<QList<float>> values_per_corte, int min){
    float corte_cap_laminar=0;
    for (int i=0;i<min;i++) {
        for (int j=0;j<6;j++)//funcion que recorrera valores.
        {
            //qDebug()<<"wc resistivo "<<values_per_corte[j][i];
            corte_cap_laminar=corte_cap_laminar+(values_per_corte[i][j]*area_sen[j]);
        }
    }
    return corte_cap_laminar/min;
}

//CAP LAMINAR EMULSION INVERSA PARA TODOOOOOOOOOOOOOOOOOO
float jptmodel::cap_laminar_EI_all(QList<QList<float>> values_per_corte, int min){
    float corte_cap_laminar=0;
    for (int i=0;i<min;i++) {
        for (int j=0;j<6;j++)//funcion que recorrera valores.
        {
            //qDebug()<<"wc capacitivo model "<<values_per_corte[i][j]<<" i " <<i <<" j "<<j;
            corte_cap_laminar+=(values_per_corte[i][j]*area_sen[j]);
        }
    }
    return corte_cap_laminar/min;
}

//
double jptmodel::res_turbulent(double values_per_corte[6])
{
    //el corte turbulento es el promedio de todos los cortes.
    //es decir, Turbulent corte=[Corte1+corte2+..+corte6]/6
    double corte_res_turbulent=0;
    for (int j=0;j<6;j++)
    {
        corte_res_turbulent=corte_res_turbulent+values_per_corte[j];
    }
    corte_res_turbulent=corte_res_turbulent/6;//la suma de todos los cortes se divide entre la cantidad de nodos.

    return  corte_res_turbulent;

}

double jptmodel::cap_turbulent(double values_per_corte[6])
{
    //el corte turbulento es el promedio de todos los cortes.
    //es decir, Turbulent corte=[Corte1+corte2+..+corte6]/6
    double corte_cap_turbulent=0;
    for (int j=0;j<6;j++)
    {
        corte_cap_turbulent=corte_cap_turbulent+values_per_corte[j];
    }


    corte_cap_turbulent=corte_cap_turbulent/6;//la suma de todos los cortes se divide entre la cantidad

    return  corte_cap_turbulent;
}

//
double jptmodel::res_turbulent_laminar(double values_per_corte[6])
{
    //el corte turbulento es el promedio de todos los cortes.
    //es decir, Turbulent corte=[Corte1+corte2+..+corte6]/6
    double corte_res_turbulent=0;
    for (int j=0;j<6;j++){
        if(j!=0){
            if(j!=3){
                corte_res_turbulent=corte_res_turbulent+values_per_corte[j];
            }
        }
    }
    corte_res_turbulent=corte_res_turbulent/4;//la suma de todos los cortes se divide entre la cantidad de nodos.

    return  corte_res_turbulent;

}

double jptmodel::cap_turbulent_laminar(double values_per_corte[6])
{
    //el corte turbulento es el promedio de todos los cortes.
    //es decir, Turbulent corte=[Corte1+corte2+..+corte6]/6
    double corte_cap_turbulent=0;
    for (int j=0;j<6;j++){
        if(j!=0){
            if(j!=3){
                corte_cap_turbulent=corte_cap_turbulent+values_per_corte[j];
            }
        }
    }
    corte_cap_turbulent=corte_cap_turbulent/4;//la suma de todos los cortes se divide entre la cantidad

    return  corte_cap_turbulent;
}


//RES TURBULENT PARA TODOOOOOOOO
float jptmodel::res_turbulent_all(QList<QList<float>> values_per_corte, int min){   
    float corte_res_turbulent=0;
    float final_res_turbulent=0;
    for (int i=0;i<min;i++) {
        corte_res_turbulent=0;
        for (int j=0;j<6;j++)//funcion que recorrera valores.
        {
            //qDebug()<<"wc resistivo "<<values_per_corte[j][i];
            corte_res_turbulent+=values_per_corte[i][j];
        }
        final_res_turbulent+=corte_res_turbulent/6;
    }
    //todos los valores se guardan en la variable corte_res_laminar,
    //es la mism que regresa el sistema con la info
    return final_res_turbulent/min;
}

//
//CAP TURBULENT PARA TODOOOOOOOOOOOOOOOOOO
float jptmodel::cap_turbulent_all(QList<QList<float>> values_per_corte, int min){
    float corte_cap_turbulent=0;
    float final_cap_turbulent=0;
    for (int i=0;i<min;i++) {
        corte_cap_turbulent=0;
        for (int j=0;j<6;j++)//funcion que recorrera valores.
        {
            //qDebug()<<"wc capacitivo model "<<values_per_corte[i][j]<<" i " <<i <<" j "<<j;
            corte_cap_turbulent+=values_per_corte[i][j];
        }
        final_cap_turbulent+=corte_cap_turbulent/6;
    }
    return final_cap_turbulent/min;
}

//

//RES TURBULENT PARA TODOOOOOOOO
float jptmodel::res_lam_turbulent_all(QList<QList<float>> values_per_corte, int min){   
    float corte_res_turbulent=0;
    float final_res_turbulent=0;
    for (int i=0;i<min;i++) {
        corte_res_turbulent=0;
        for (int j=0;j<6;j++)//funcion que recorrera valores.
        {
            //qDebug()<<"wc resistivo "<<values_per_corte[j][i];
            //evadimo el nodo 1 y 3
            if(j!=0){
                if(j!=3){
                    corte_res_turbulent+=values_per_corte[i][j];
                }
            }
        }
        final_res_turbulent+=corte_res_turbulent/4;
    }
    //todos los valores se guardan en la variable corte_res_laminar,
    //es la mism que regresa el sistema con la info
    return final_res_turbulent/min;
}

//
//CAP TURBULENT PARA TODOOOOOOOOOOOOOOOOOO
float jptmodel::cap_lam_turbulent_all(QList<QList<float>> values_per_corte, int min){
    float corte_cap_turbulent=0;
    float final_cap_turbulent=0;
    for (int i=0;i<min;i++) {
        corte_cap_turbulent=0;
        for (int j=0;j<6;j++)//funcion que recorrera valores.
        {
            //qDebug()<<"wc capacitivo model "<<values_per_corte[i][j]<<" i " <<i <<" j "<<j;
            if(j!=0){
                if(j!=3){
                    corte_cap_turbulent+=values_per_corte[i][j];
                }
            }
        }
        final_cap_turbulent+=corte_cap_turbulent/4;
    }
    return final_cap_turbulent/min;
}

//

#include <QDebug>
//******************************************************************************************************************************
// Aplica la curva de calculo de los capacitivos y resistivos en el modelo turbulento                                 [jptmodel]
//------------------------------------------------------------------------------------------------------------------------------
void jptmodel::aplicate_high_model(QVector<int> res_sensor, QVector<int> cap_sensor, double *hcut_tur_cap, double *hcut_tur_res, double factor, int x_1){
    for(int f_sens = 0; f_sens < def_num_sens; ++f_sens){
        *hcut_tur_cap += (factor*cap_sensor[f_sens]) + ((factor*-1*x_1) + 100);
        *hcut_tur_res += select_model_mixture_res(res_sensor[f_sens]);
    }
    *hcut_tur_cap /= def_num_sens;
    *hcut_tur_res /= def_num_sens;
}
//******************************************************************************************************************************
// Aplica la curva de mezclas para los sensores capacitivos                                                           [jptmodel]
//******************************************************************************************************************************
double jptmodel::select_model_mixture_cap(int raw_cap_sen){
    double cap_mixture(0);

    if(raw_cap_sen < def_lim_inf_cap_070_100)
        cap_mixture = def_value_max_porcentaje;
    else if(raw_cap_sen >= def_lim_inf_cap_070_100 && raw_cap_sen < def_lim_sup_cap_070_100)
        cap_mixture = cap_mixture_model_070_100(raw_cap_sen);
    else if(raw_cap_sen >= def_lim_sup_cap_070_100 && raw_cap_sen < def_lim_sup_cap_040_070)
        cap_mixture = cap_mixture_model_040_070(raw_cap_sen);
    else if(raw_cap_sen >= def_lim_sup_cap_040_070 && raw_cap_sen < def_lim_sup_cap_030_040)
        cap_mixture = cap_mixture_model_030_040(raw_cap_sen);
    else if(raw_cap_sen >= def_lim_sup_cap_030_040 && raw_cap_sen < def_lim_sup_cap_000_030)
        cap_mixture = cap_mixture_model_000_030(raw_cap_sen);

    return cap_mixture;
}

//******************************************************************************************************************************
// Aplica la curva de mezclas para los sensores resistivos                                                            [jptmodel]
//******************************************************************************************************************************
double jptmodel::select_model_mixture_res(int raw_res_sen){
    double value = (double) def_res_mixture_e1*raw_res_sen + def_res_mixture_e2;

    if(value < def_value_min_porcentaje)
        value = def_value_min_porcentaje;
    if(value > def_value_max_porcentaje)
        value = def_value_max_porcentaje;

    return value;

}

//******************************************************************************************************************************
// Curvas de porcentaje de corte para mezclas en los sensores capacitivos                                             [jptmodel]
//******************************************************************************************************************************
double jptmodel::res_mixture_model_000_100(int raw){
    return (double) raw;
}
//******************************************************************************************************************************
// Curvas de porcentaje de corte para mezclas en los sensores capacitivos                                             [jptmodel]
//******************************************************************************************************************************
//---------------------------------------------------------------------------------------------
// Polinomio de mezclas en los capacitivos, para alto corte: entre 70 - 100 %       PDF: Morado
//---------------------------------------------------------------------------------------------
double jptmodel::cap_mixture_model_070_100(int raw){
    return  (double)def_cap_mixture_e1_70*exp(def_cap_mixture_e2_70*raw);
}
//---------------------------------------------------------------------------------------------
// Polinomio de mezclas en los capacitivos, para alto medio corte: entre 40 - 70 %    PDF: Rojo
//---------------------------------------------------------------------------------------------
double jptmodel::cap_mixture_model_040_070(int raw){
    return  (double)def_cap_mixture_e1_40*pow(raw, 3) + def_cap_mixture_e2_40*pow(raw,2) + def_cap_mixture_e3_40*raw + def_cap_mixture_e4_40;
}
//---------------------------------------------------------------------------------------------
// Polinomio de mezclas en los capacitivos, medio corte: entre 30 - 40 %              PDF: Azul
//---------------------------------------------------------------------------------------------
double jptmodel::cap_mixture_model_030_040(int raw){
    return  (double)def_cap_mixture_e1_30*raw + def_cap_mixture_e2_30;
}
//---------------------------------------------------------------------------------------------
// Polinomio de mezclas en los capacitivos, bajo corte: entre 00 - 30 %              PDF: Verde
//---------------------------------------------------------------------------------------------
double jptmodel::cap_mixture_model_000_030(int raw){
    return  (double)def_cap_mixture_e1_00*pow(raw, 3) + def_cap_mixture_e2_00*pow(raw,2) + def_cap_mixture_e3_00*raw + def_cap_mixture_e4_00;
}
//---------------------------------------------------------------------------------------------
// Fin
//---------------------------------------------------------------------------------------------
//******************************************************************************************************************************
// Curvas de porcentaje de corte para mezclas en los sensores resistivos                                              [jptmodel]
//******************************************************************************************************************************
