#ifndef JPTRESSENSOR_H
#define JPTRESSENSOR_H

#include "jptsensors.h"

//-----------------------------------------------------------------------------------------------------------------------------
// Polinomio de compensacion de cuentas por temperatura par valores menores a 8000, que corresponden a Agua
// y = e1 + e2 * raw
//-----------------------------------------------------------------------------------------------------------------------------
#define def_eq_res_compensate_temp_e1     6631.1
#define def_eq_res_compensate_temp_e2    -45.311

//-----------------------------------------------------------------------------------------------------------------------------
// Polinomio de compensacion de cuentas por flujo para valores menores a 8000, que corresponden a Agua
// y = e1 + e2 * raw
//-----------------------------------------------------------------------------------------------------------------------------
#define def_eq_res_compensate_flow_e1     6631.1
#define def_eq_res_compensate_flow_e2    -45.311

//-----------------------------------------------------------------------------------------------------------------------------
// Polinomio de compesacion de cuentas por salinidad para valores menores a 8000, que corresponden a agua.
// y = e1 + (e2 * exp(- raw / e3))
//-----------------------------------------------------------------------------------------------------------------------------
#define def_eq_res_compensate_sali_e1 -323.220
#define def_eq_res_compensate_sali_e2  3891.34
#define def_eq_res_compensate_sali_e3  1952.78

//-----------------------------------------------------------------------------------------------------------------------------
// Definicion de cuentas -base- para una temperatura de 25C
//-----------------------------------------------------------------------------------------------------------------------------
#define def_count_base_res_temp 5498

//-----------------------------------------------------------------------------------------------------------------------------
// Definicion de cuentas -base- para una salinidad de 0ppm
//--------------------------------------------------------compensate_temperature---------------------------------------------------------------------
#define def_count_base_res_sali  5673

//-----------------------------------------------------------------------------------------------------------------------------
// Definicion de cuentas -base- para un flujo xx gl
//-----------------------------------------------------------------------------------------------------------------------------
#define def_count_base_res_flow  5673

//-----------------------------------------------------------------------------------------------------------------------------
// Ecuacion para el calculo de la salinidad [para sensores 1 y 6]
// y = e1 + (e2 * exp(- raw / e3))
//-----------------------------------------------------------------------------------------------------------------------------
#define def_eq_value_salinity_e1 -323.220
#define def_eq_value_salinity_e2  3891.34
#define def_eq_value_salinity_e3  1952.78
//-----------------------------------------------------------------------------------------------------------------------------

class jptressensor : public jptsensors{

public:
    jptressensor();
    ~jptressensor();


    //******************************************************************************************************************************
    // Calculo del delta cuenta para el sensor resistivo en base a al flujo
    //******************************************************************************************************************************
    static void calcule_delta_cuentas_flow(const double flow, int *delta_cuentas){
        double c_compensate_flow  = (double)(def_eq_res_compensate_flow_e1 + def_eq_res_compensate_flow_e2 * flow);
        *delta_cuentas = (int)(def_count_base_res_flow - c_compensate_flow);
    }

    //******************************************************************************************************************************
    // Calculo del delta cuenta para el sensor resistivo en base a la temperatura
    //******************************************************************************************************************************
    static void calcule_delta_cuentas_temp(const double temperature, int *delta_cuentas){
        double c_compensate_temp  = (double)(def_eq_res_compensate_temp_e1 + def_eq_res_compensate_temp_e2 * temperature);
        *delta_cuentas = (int)(def_count_base_res_flow - c_compensate_temp);
    }
    static int calcule_cuentas_temp(double temperature)
    {   double c_compensate_temp   = (double)(def_eq_res_compensate_temp_e1 + (def_eq_res_compensate_temp_e2) * temperature);
        int _cuentas= (int)(def_count_base_res_temp - c_compensate_temp);
        return _cuentas;

    }

    //******************************************************************************************************************************
    // Calculo del delta cuenta para el sensor resistivo en base a la temperatura
    //******************************************************************************************************************************
    static void calcule_delta_cuentas_sali(double salinity, int *delta_cuentas){
        double c_compensate_sali   = (double) def_eq_res_compensate_sali_e1 + (def_eq_res_compensate_sali_e2 * exp(-1 * salinity/def_eq_res_compensate_sali_e3));
        *delta_cuentas             = (int)(def_count_base_res_sali - c_compensate_sali);
    }
    static int calcule_delta_sali(double salinity)
    {
        double compensate_sali=(double) def_eq_res_compensate_sali_e1 + (def_eq_res_compensate_sali_e2 * exp(-1 * salinity/def_eq_res_compensate_sali_e3));
        int _cuentas=(int)(def_count_base_res_sali - compensate_sali);
        return _cuentas;

    }

    //******************************************************************************************************************************
    // Calculando el valor de la salinidad para los sensores resistivos [1] y/o [6]  (ecuacicon)[determine_real_salinity:MainWindow]
    //******************************************************************************************************************************
    static void calcule_physic_salinity(int *salinity, int raw){
        *salinity = def_eq_value_salinity_e1 + (def_eq_value_salinity_e2 * exp(-1 * raw/ def_eq_value_salinity_e3));
    }
    static int calcule_salinity_temp(int raw)
    {   int _salinity = def_eq_value_salinity_e1 + (def_eq_value_salinity_e2 * exp(-1 * (raw/ def_eq_value_salinity_e3)));
        return _salinity;
    }

    //new function calcule salinity
    static int calcule_new_salinity(int raw){
        int salinity = 5878.1 * exp(-0.00006 * raw);
        return salinity;
    }
};

#endif // JPTRESSENSOR_H
