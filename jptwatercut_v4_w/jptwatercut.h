#ifndef JPTWATERCUT_H
#define JPTWATERCUT_H

#include <QMainWindow>
#include <QTimer>
#include "communication/jpttcpclient.h"
#include "logic/jptabssensor.h"
#include "logic/jptcapsensor.h"
#include "logic/jptmodel.h"
#include "logic/jptressensor.h"
#include "logic/jptsensors.h"

QT_BEGIN_NAMESPACE
namespace Ui { class jptwatercut; }
QT_END_NAMESPACE

class jptwatercut : public QMainWindow
{
    Q_OBJECT

public:
    jptwatercut(QWidget *parent = nullptr);
    ~jptwatercut();

    void read_txt_values_calibration();
    void read_txt_values_high_low_wc();
    void read_txt_delta_normalization();
    void read_txt_inverse();
    void read_txt_inverse_values();
    void read_txt_correct_sali();
    void read_txt_correct_temp();
    void read_txt_correct_tempval();
    void charge_line_norm();
    void validator_line_norm();
    void charge_models_tempe_pres();
    void charge_lcd();
    void fill_lcd_raw(QStringList _raw);
    void normalize_raw(QStringList _raw);
    void salinity();
    void watercut_res_laminar();
    void watercut_cap_laminar();
    void calcule_model_laminar();
    void regimen_laminar();
    //void watercut_res_laminar_ei();
    void watercut_cap_laminar_ei();
    void regimen_laminar_ei();
    void calcule_model_laminar_ei();
    void watercut_res_turbulent();
    void watercut_cap_turbulent();
    int subs_values(int value1, int value2);
    void calcule_model_turbulent();
    void regimen_turbulent();
    void watercut_res_turbulent_lam();
    void watercut_cap_turbulent_lam();
    void calcule_model_turbulent_lam();
    void regimen_turbulent_lam();
    void fill_data_auto();
    void smart_model_id();
    void execute_regimen();

public slots:
    void start_communication();
    void receive_json_from_tcpclient(QStringList _raw,QStringList _eng, QStringList _sensors);
    void restart_communication();
    void save_txt_delta_normalization();
    void select_auto_mode(bool mode);
    void select_manual_mode(bool mode);
    void take_samples();
    void active_manual();
    void select_manual_laminar();
    void select_manual_laminar_ei();
    void select_manual_turbulent();
    void select_manual_turbulent_laminar();
    void put_message_console_smart(QString msg);

signals:
    void send_message_console_smart(QString msg);

private:
    Ui::jptwatercut *ui;
    QTimer *a_open_conection;
    jpttcpclient *a_tcp_client;
    jptmodel *model;
    jptsensors *sensors;
    jptabssensor *abs_sensors;
    jptcapsensor *cap_sensors;
    jptressensor *res_sensors;

signals:
    void start_connection(QString);


};
#endif // JPTWATERCUT_H
