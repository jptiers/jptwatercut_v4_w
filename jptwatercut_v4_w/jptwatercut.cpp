#include "jptwatercut.h"
#include "ui_jptwatercut.h"

static int vector_values_calibration[8];
//Average--res_oil[0],res_air[1],res_water[2],res_f_water[3]
//Average--cap_oil[4],cap_air[5],cap_water[6],cap_f_water[7]
static int vector_high_low_wc[12];
//Percent--hwc_up[0],Value--hwc_up[1]
//Percent--hwc_dw[2],Value--hwc_dw[3]
//Percent--lwc_up[4],Value--lwc_up[5]
//Percent--lwc_dw[6],Value--lwc_dw[7]
//Percent--hwc_up2[8],Value--hwc_up2[9]
//Percent--hwc_dw2[10],Value--hwc_dw2[11]
static int vector_delta_normalization[12];
static int on_off_inverse, on_off_sali, on_off_temp, on_off_tempval;
static float value_tempval;
//On->1, Off->0
/*!*/
static int vector_inverse_values[12];
//normalization
static int vector_normalize_res[6];
static int vector_normalize_cap[6];
static QVector<QLineEdit *> vector_line_edit_norm;
static QVector<jptabssensor *> vector_abs_sensors;
static QVector<QLCDNumber *> vector_lcd;
static float real_salinity = 0;

static double vector_watercut_res_laminar[6];
static double vector_watercut_cap_laminar[6];
static float res_laminar = 0;
static float cap_laminar = 0;

static float vector_factor_res_laminar_ei[6];
static float vector_factor_cap_laminar_ei[6];
static double vector_watercut_cap_laminar_ei[6];
static float cap_laminar_ei = 0;

static double vector_watercut_res_turbulent[6];
static double vector_watercut_cap_turbulent[6];
static float res_turbulent = 0;
static float cap_turbulent = 0;

static double vector_watercut_res_turbulent_lam[6];
static double vector_watercut_cap_turbulent_lam[6];
static float res_lam_turbulent = 0;
static float cap_lam_turbulent = 0;

static QVector<QList <int >> vector_smart_data_res;
static QVector<QList <int >> vector_smart_data_cap;
static bool auto_mode_id = false;
static bool manual_mode_id = false;
static bool taking_samples = false;
static int counter_samples = 0;
static bool smart_mode = false;
static int counter_msg = 0;

static bool laminar=false, lei=false, turbulent=false, turb_lam=false;

jptwatercut::jptwatercut(QWidget *parent) : QMainWindow(parent), ui(new Ui::jptwatercut){
    ui->setupUi(this);

    //----------------------------------------------------------------------------------------------------------------
    // Timer que permite abrir la comunicacion con jptmonitorig TCP                            [QTimer - jpttcpclient]
    //----------------------------------------------------------------------------------------------------------------
    a_open_conection = new QTimer();
    a_open_conection->setInterval(4000);
    a_open_conection->setSingleShot(true);
    a_open_conection->start();

    //----------------------------------------------------------------------------------------------------------------
    // Comunicacion TCP con JPTMonitoring                                                               [jpttcpclient]
    //----------------------------------------------------------------------------------------------------------------
    a_tcp_client = new jpttcpclient(this);
    connect(a_open_conection, SIGNAL(timeout()), this, SLOT(start_communication()));
    connect(this, SIGNAL(start_connection(QString)), a_tcp_client, SLOT(send_message(QString)));
    connect(a_tcp_client, SIGNAL(closed_conection()), this, SLOT(restart_communication()));
    connect(a_tcp_client, SIGNAL(new_json_entry(QStringList,QStringList,QStringList)), this, SLOT(receive_json_from_tcpclient(QStringList,QStringList,QStringList)));

    //Set title in window
    setWindowTitle("SWC V4.0");

    setFixedSize(800,600);

    //Read init calibration values
    read_txt_values_calibration();
    //Read init hlwc values
    read_txt_values_high_low_wc();
    //Read init delta normalization values
    read_txt_delta_normalization();
    //Read init charge inverse on, off
    read_txt_inverse();
    //Read init inverse values
    read_txt_inverse_values();
    //Read init charge sali on, off
    read_txt_correct_sali();
    //Read init charge temp on, off
    read_txt_correct_temp();
    //Read init charge temp_val on, off and value
    read_txt_correct_tempval();

    charge_line_norm();
    connect(ui->btn_save_norm, SIGNAL(clicked()), this, SLOT(save_txt_delta_normalization()));

    charge_models_tempe_pres();
    charge_lcd();
    on_off_inverse=0;

    connect(ui->radio_auto_id, SIGNAL(clicked(bool)), this, SLOT(select_auto_mode(bool)));
    connect(ui->radio_manual_id, SIGNAL(clicked(bool)), this, SLOT(select_manual_mode(bool)));
    connect(ui->btn_active_manual, SIGNAL(clicked()), this, SLOT(active_manual()));
    connect(ui->radio_manual_lam, SIGNAL(clicked()), this, SLOT(select_manual_laminar()));
    connect(ui->radio_manual_lam_ei, SIGNAL(clicked()), this, SLOT(select_manual_laminar_ei()));
    connect(ui->radio_manual_turbulent, SIGNAL(clicked()), this, SLOT(select_manual_turbulent()));
    connect(ui->radio_manual_turbulent_lam, SIGNAL(clicked()), this, SLOT(select_manual_turbulent_laminar()));
    connect(ui->btn_take_samples, SIGNAL(clicked()), this, SLOT(take_samples()));
    connect(this, SIGNAL(send_message_console_smart(QString)), this, SLOT(put_message_console_smart(QString)));
    ui->text_console_smart->setReadOnly(true);
}

jptwatercut::~jptwatercut(){
    delete ui;
}

void jptwatercut::charge_line_norm(){
    vector_line_edit_norm.append(ui->line_norm_res1);
    vector_line_edit_norm.append(ui->line_norm_res2);
    vector_line_edit_norm.append(ui->line_norm_res3);
    vector_line_edit_norm.append(ui->line_norm_res4);
    vector_line_edit_norm.append(ui->line_norm_res5);
    vector_line_edit_norm.append(ui->line_norm_res6);
    vector_line_edit_norm.append(ui->line_norm_cap1);
    vector_line_edit_norm.append(ui->line_norm_cap2);
    vector_line_edit_norm.append(ui->line_norm_cap3);
    vector_line_edit_norm.append(ui->line_norm_cap4);
    vector_line_edit_norm.append(ui->line_norm_cap5);
    vector_line_edit_norm.append(ui->line_norm_cap6);

    validator_line_norm();
}

void jptwatercut::validator_line_norm(){
    for (int i=0;i<vector_line_edit_norm.size();i++) {
        vector_line_edit_norm[i]->setValidator(new QDoubleValidator(this));
    }
}

void jptwatercut::charge_lcd(){
    vector_lcd.append(ui->lcd_res_1);
    vector_lcd.append(ui->lcd_res_2);
    vector_lcd.append(ui->lcd_res_3);
    vector_lcd.append(ui->lcd_res_4);
    vector_lcd.append(ui->lcd_res_5);
    vector_lcd.append(ui->lcd_res_6);
    vector_lcd.append(ui->lcd_cap_1);
    vector_lcd.append(ui->lcd_cap_2);
    vector_lcd.append(ui->lcd_cap_3);
    vector_lcd.append(ui->lcd_cap_4);
    vector_lcd.append(ui->lcd_cap_5);
    vector_lcd.append(ui->lcd_cap_6);
    vector_lcd.append(ui->lcd_temp_raw);
}

void jptwatercut::fill_lcd_raw(QStringList _raw){
    for (int i=0;i<_raw.count();i++) {
        vector_lcd[i]->display(_raw[i]);
    }
}

void jptwatercut::charge_models_tempe_pres(){
    vector_abs_sensors.append(new jptabssensor);
    vector_abs_sensors.append(new jptabssensor);
}

void jptwatercut::select_auto_mode(bool mode){
    if(mode){
        ui->group_automatic_model_id->setEnabled(true);
        ui->group_manual_model_id->setEnabled(false);
        auto_mode_id = true;
        manual_mode_id = false;
    }
}

void jptwatercut::select_manual_mode(bool mode){
    if(mode){
        ui->group_automatic_model_id->setEnabled(false);
        ui->group_manual_model_id->setEnabled(true);
        manual_mode_id = true;
        auto_mode_id =  false;
        smart_mode = false;
    }
}

void jptwatercut::take_samples(){
    taking_samples = true;
    vector_smart_data_cap.clear();
    vector_smart_data_res.clear();
    counter_samples = 0;
    smart_mode = false;
}

void jptwatercut::select_manual_laminar(){
    laminar = true;
    lei=false;
    turbulent=false;
    turb_lam=false;
}

void jptwatercut::select_manual_laminar_ei(){
    laminar = false;
    lei= true;
    turbulent=false;
    turb_lam=false;
}

void jptwatercut::select_manual_turbulent(){
    laminar = false;
    lei=false;
    turbulent=true;
    turb_lam=false;
}

void jptwatercut::select_manual_turbulent_laminar(){
    laminar = false;
    lei=false;
    turbulent=false;
    turb_lam=true;
}

void jptwatercut::put_message_console_smart(QString msg){
    if(counter_msg == 50){
        ui->text_console_smart->clear();
    }else{
        ui->text_console_smart->append(msg);
        counter_msg++;
    }
}

void jptwatercut::active_manual(){
    smart_mode = false;
    manual_mode_id = true;
}
//******************************************************************************************************************************
// Funcion que inicia la conexion con JPTMonitoring                                         [SIGNAL - jpttcpclient:send_message]
//******************************************************************************************************************************
void jptwatercut::start_communication(){
    emit start_connection("Start");
}

void jptwatercut::restart_communication(){
    a_open_conection->start();
}

void jptwatercut::read_txt_values_high_low_wc(){
    QString path("");

    path = "/jpt/jptwatercut/External/hlwc.txt";

    QFile file(path);
    if(!file.exists()){
        qDebug() << "No existe el archivo de hlwc...";
    }else{
        QString line;
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
            QTextStream stream(&file);
            int record=0;
            while (!stream.atEnd() && record<12){
                line = stream.readLine();
                vector_high_low_wc[record]=line.toInt();
                record++;
            }
            record=0;
        }
        file.close();
    }
}

void jptwatercut::read_txt_delta_normalization(){
    QString path("");

    path = "/jpt/jptwatercut/External/delta_n.txt";

    QFile file(path);
    if(!file.exists()){
        qDebug() << "No existe el archivo de delta_n...";
    }else{
        QString line;
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
            QTextStream stream(&file);
            int record=0;
            while (!stream.atEnd() && record<12){
                line = stream.readLine();
                vector_delta_normalization[record]=line.toInt();
                //vector_line_edit_norm[record]->setText(line);
                record++;
            }
            record=0;
        }
        file.close();
    }
}

void jptwatercut::save_txt_delta_normalization(){
    //Create route where this located the file
    QString path("");

    path = "/jpt/jptwatercut/External/delta_n.txt";

    //Evaluate if exist file
    QFile file(path);
    //Apply in the conditional
    if(!file.exists()){
        //If not exist file, create one
        QFile create_file(path);
        if(create_file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append)){
            //How is the first time
            //qDebug()<<"PRIMERA VEZ CONFIG";
            QTextStream input_data(&create_file);
            //Write data into file after created
            //primero va de resistividad 1 al 6 y luego de cap 1 al 6
            for(int i=0;i<vector_line_edit_norm.size();i++){
                if(vector_line_edit_norm[i]->text() != ""){
                    input_data<<vector_line_edit_norm[i]->text()<<endl;
                }else{
                    input_data<<0<<endl;
                }
            }
            //Close file
            create_file.close();
        }
    }else{
        if(file.open(QIODevice::ReadWrite | QIODevice::Truncate | QIODevice::Text)){
            QTextStream datosArchivo(&file);
            //datosArchivo <<"";
            for(int i=0;i<vector_line_edit_norm.size();i++){
                if(vector_line_edit_norm[i]->text() != ""){
                    datosArchivo<<vector_line_edit_norm[i]->text()<<endl;
                }else{
                    datosArchivo<<0<<endl;
                }
            }
        }
        file.close();
    }
    for(int i=0;i<vector_line_edit_norm.size();i++){
        vector_delta_normalization[i] = vector_line_edit_norm[i]->text().toInt();
    }
    //normalization_data();
}

void jptwatercut::read_txt_inverse(){
    QString path("");

    path = "/jpt/jptwatercut/External/inverse.txt";

    QFile file(path);
    if(!file.exists()){
        qDebug() << "No existe el archivo de inverse...";
    }else{
        QString line;
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
            QTextStream stream(&file);
            int record=0;
            while (!stream.atEnd() && record<1){
                line = stream.readLine();
                on_off_inverse = line.toInt();
            }
        }
        file.close();
    }
}

void jptwatercut::read_txt_inverse_values(){
    QString path("");

    path = "/jpt/jptwatercut/External/inverse_values.txt";

    QFile file(path);
    if(!file.exists()){
        qDebug() << "No existe el archivo de inverse_values...";
    }else{
        QString line;
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
            QTextStream stream(&file);
            int record=0;
            while (!stream.atEnd() && record<12){
                line = stream.readLine();
                vector_inverse_values[record] = line.toInt();
                record++;
            }
        }
        file.close();
    }
}

void jptwatercut::read_txt_correct_sali(){
    QString path("");

    path = "/jpt/jptwatercut/External/sali.txt";

    QFile file(path);
    if(!file.exists()){
        qDebug() << "No existe el archivo de sali...";
    }else{
        QString line;
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
            QTextStream stream(&file);
            int record=0;
            while (!stream.atEnd() && record<1){
                line = stream.readLine();
                on_off_sali = line.toInt();
            }
        }
        file.close();
    }
}

void jptwatercut::read_txt_correct_temp(){
    QString path("");

    path = "/jpt/jptwatercut/External/sali.txt";

    QFile file(path);
    if(!file.exists()){
        qDebug() << "No existe el archivo de temp...";
    }else{
        QString line;
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
            QTextStream stream(&file);
            int record=0;
            while (!stream.atEnd() && record<1){
                line = stream.readLine();
                on_off_temp = line.toInt();
            }
        }
        file.close();
    }
}

void jptwatercut::read_txt_correct_tempval(){
    QString path("");

    path = "/jpt/jptwatercut/External/temp.txt";

    QFile file(path);
    if(!file.exists()){
        qDebug() << "No existe el archivo de tempval...";
    }else{
        QString line;
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
            QTextStream stream(&file);
            int record=0;
            while (!stream.atEnd() && record<2){
                line = stream.readLine();
                if(record == 0){
                    on_off_tempval = line.toInt();
                }else{
                    value_tempval = line.toFloat();
                }
            }
            record++;
        }
        file.close();
    }
}

void jptwatercut::read_txt_values_calibration(){
    QString path("");

    path = "/jpt/jptwatercut/External/calibration_values.txt";

    QFile file(path);
    if(!file.exists()){
        qDebug() << "No existe el archivo de calibracion...";
    }else{
        QString line;
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
            QTextStream stream(&file);
            int record=0;
            while (!stream.atEnd() && record<8){
                line = stream.readLine();
                vector_values_calibration[record]=line.toInt();
                record++;
            }
            record=0;
        }
        file.close();
    }
}

void jptwatercut::normalize_raw(QStringList _raw){
    if(ui->check_norm_data->isChecked()){
        for(int i=0;i<6;i++){
            vector_normalize_res[i] = vector_delta_normalization[i] + _raw[i].toInt();
            vector_normalize_cap[i] = vector_delta_normalization[i+6] + _raw[i+6].toInt();
        }
    }else{
        for(int i=0;i<6;i++){
            vector_normalize_res[i] = _raw[i].toInt();
            vector_normalize_cap[i] = _raw[i+6].toInt();
        }
    }
}

void jptwatercut::salinity(){
    //Variables temporales raw
    float comp_resis_r1 = 0, comp_resis_r6 = 0;
    int raw_resis_r1 = vector_normalize_res[0];
    int raw_resis_r6 = vector_normalize_res[5];

    //Condicional en caso de los dos sensores resistivos 1,6
    if(ui->check_com_sal_r1->isChecked() && ui->check_com_sal_r6->isChecked()){
        //En caso que solo sea por parte de lo que llega
        //salinity
        comp_resis_r1 = res_sensors->calcule_new_salinity(raw_resis_r1);
        comp_resis_r6 = res_sensors->calcule_new_salinity(raw_resis_r6);

        //No pueden ser valores negativos

        if(comp_resis_r1 < 0){
            //comp_resis_r1 = 0;
            comp_resis_r1=abs(comp_resis_r1);
        }
        if(comp_resis_r6 < 0){
            //comp_resis_r6 = 0;
            comp_resis_r6=abs(comp_resis_r1);
        }
        //qDebug()<<"Salinidad R1 "<<comp_resis_r1;
        //qDebug()<<"Salinidad R6 "<<comp_resis_r6;
        float avg = (comp_resis_r1+comp_resis_r6)/2;
        real_salinity = avg;
    }else if(ui->check_com_sal_r1->isChecked() && !ui->check_com_sal_r6->isChecked()){
        //Condicional en caso de los dos sensores resistivo 1
        //En caso que solo sea por parte de lo que llega
        //salinity
        comp_resis_r1 = res_sensors->calcule_new_salinity(raw_resis_r1);

        //No pueden ser valores negativos
        if(comp_resis_r1 < 0){
            //comp_resis_r1 = 0;
            comp_resis_r1=abs(comp_resis_r1);
        }
        //qDebug()<<"Salinidad R1 "<<comp_resis_r1;
        real_salinity = comp_resis_r1;
    }else if(!ui->check_com_sal_r1->isChecked() && ui->check_com_sal_r6->isChecked()){
        //Condicional en caso de los dos sensores resistivo 1
        //En caso que solo sea por parte de lo que llega
        //salinity
        comp_resis_r6 = res_sensors->calcule_new_salinity(raw_resis_r6);
        //No pueden ser valores negativos
        if(comp_resis_r6 < 0){
            //comp_resis_r6 = 0;
            comp_resis_r6=abs(comp_resis_r6);
        }
        //qDebug()<<"Salinidad R6 "<<comp_resis_r6;
        real_salinity = comp_resis_r6;
    }else{
        real_salinity = 0;
    }
}

void jptwatercut::watercut_res_laminar(){
    int resis_water = vector_values_calibration[2];
    int resis_oil= vector_values_calibration[0];
    int total_counts_res=0;

    //Pendiente
    float m = (-100)/float((resis_oil-resis_water));
    //Vector Y0
    float Y0 = float((-m*resis_water)+100);

    for (int i=0;i<6;i++) {
        total_counts_res = vector_normalize_res[i];

        float watercut_resis = float((m*total_counts_res)+Y0);
        if(watercut_resis>=100){
            watercut_resis = 100;
        }else if(watercut_resis<=0){
            watercut_resis = 0;
        }
        vector_watercut_res_laminar[i] = watercut_resis;
    }
    //qDebug()<<"total res lam "<<list_wc_res[i]<<" i "<<i;
}

void jptwatercut::watercut_cap_laminar(){
    //esta funcion calculara y generara la formula de capacitancia. la cual cuenta de 4 funciones,3 staticas y 1 variables
    //las funciones estaticas estan definidas en JPTModel como cap_mixture y d estas se tomaran la funcion de 0a 30,
    //la funcion de 30 a 40 y de 40 a70%
    //las funciones siempre necesitan una valor de capacitancia para retornar el valor del porcentaje de agua equivalente.
    //Ahora bien, primero obtenemos el valor para el calculo de la grafica. Ccwater y CcformationWater

    int capa_water = vector_values_calibration[6];
    int capa_water_f = vector_values_calibration[7];
    //con estos valores debo de generar mi curva dinamica.
    /*
         * NOTA: Para generar la curva se necesitan dos puntos, en este caso serian (100,CFw) y (X,Cw)
         * el valor de X en mi punto P2 esta dado por el porcentaje de agua que existe en ese punto,
         * para ello se evalue la funcion cap_mixture_40_70 del jptmodel en CW y con eso obtenems el valor de x.
         *
         */
    float percent_p2 = float(model->cap_mixture_model_040_070(capa_water));
    //la siguiente e la funcion dinamica que toma el valor de X[i] y calcula los valores.
    //funcion=((-1)*(percent_p2-100)/(CWater-CF_Water))*(CWater-X[i])+percent_p2;


    for (int i=0;i<6;i++) {
        int raw_norm_cap=0;

        raw_norm_cap = vector_normalize_cap[i];

        float watercut_for_sensor = ((-1)*(percent_p2-100)/(capa_water-capa_water_f))*(capa_water-capa_water_f)+percent_p2;


        if(raw_norm_cap<=capa_water_f){
            //todo antes de las formaciones es agua. entonces f(x) seria 100% agua.
            raw_norm_cap=100;
        }
        else{
            if(raw_norm_cap<=capa_water){
                // no haga nada es el valor actual de wc_per_sensor con la curva dinamica
            }
            else{
                if(raw_norm_cap<=7501){
                    watercut_for_sensor = model->cap_mixture_model_040_070(raw_norm_cap);
                }
                else{
                    if(raw_norm_cap<=13801){
                        watercut_for_sensor = model->cap_mixture_model_030_040(raw_norm_cap);
                    }
                    else{
                        if(raw_norm_cap<=15001){
                            watercut_for_sensor = model->cap_mixture_model_000_030(raw_norm_cap);
                        }
                        else{
                            watercut_for_sensor=0;
                        }
                    }
                }
            }
        }
        if(watercut_for_sensor>100){
            watercut_for_sensor = 100;
            vector_watercut_cap_laminar[i] = watercut_for_sensor;
        }else{
            vector_watercut_cap_laminar[i] = watercut_for_sensor;
        }
        //qDebug()<<"wc capacitivo "<<watercut_for_sensor;
    }

}

void jptwatercut::calcule_model_laminar(){
    res_laminar = model->res_laminar(vector_watercut_res_laminar);

    cap_laminar = model->cap_laminar(vector_watercut_cap_laminar);

    qDebug()<<"res laminar "<<res_laminar;
    qDebug()<<"cap laminar "<<cap_laminar;
}

void jptwatercut::regimen_laminar(){
    //se agrega la parte de validacion desde el threshold para agua y oil
    if(vector_normalize_res[0]<ui->spin_value_th_water_lam->value() && vector_normalize_res[5]>ui->spin_value_th_oil_lam->value() &&
            vector_normalize_cap[0]<ui->spin_value_th_water_lam->value() && vector_normalize_cap[5]>ui->spin_value_th_oil_lam->value()){
        qDebug()<<"LAMINAR";
        watercut_res_laminar();
        watercut_cap_laminar();
        calcule_model_laminar();

        //calcule_model_laminar_EI();
    }else{
        qDebug()<<"No cumple para laminar";
    }
}

void jptwatercut::watercut_cap_laminar_ei(){
    //las funciones estaticas estan definidas en JPTModel como cap_mixture y d estas se tomaran la funcion de 0a 30,
    //la funcion de 30 a 40 y de 40 a70%
    //las funciones siempre necesitan una valor de capacitancia para retornar el valor del porcentaje de agua equivalente.
    //Ahora bien, primero obtenemos el valor para el calculo de la grafica. Ccwater y CcformationWater

    int capa_water = vector_values_calibration[6];
    int capa_water_f = vector_values_calibration[7];
    //con estos valores debo de generar mi curva dinamica.
    /*
         * NOTA: Para generar la curva se necesitan dos puntos, en este caso serian (100,CFw) y (X,Cw)
         * el valor de X en mi punto P2 esta dado por el porcentaje de agua que existe en ese punto,
         * para ello se evalue la funcion cap_mixture_40_70 del jptmodel en CW y con eso obtenems el valor de x.
         *
         */
    float percent_p2 = float(model->cap_mixture_model_040_070(capa_water));
    //la siguiente e la funcion dinamica que toma el valor de X[i] y calcula los valores.
    //funcion=((-1)*(percent_p2-100)/(CWater-CF_Water))*(CWater-X[i])+percent_p2;


    for (int i=0;i<6;i++) {
        int raw_norm_cap=0;

        raw_norm_cap = vector_normalize_cap[i];

        float watercut_for_sensor = ((-1)*(percent_p2-100)/(capa_water-capa_water_f))*(capa_water-capa_water_f)+percent_p2;


        if(raw_norm_cap<=capa_water_f){
            //todo antes de las formaciones es agua. entonces f(x) seria 100% agua.
            raw_norm_cap=100;
        }
        else{
            if(raw_norm_cap<=capa_water){
                // no haga nada es el valor actual de wc_per_sensor con la curva dinamica
            }
            else{
                if(raw_norm_cap<=7501){
                    watercut_for_sensor = model->cap_mixture_model_040_070(raw_norm_cap);
                }
                else{
                    if(raw_norm_cap<=13801){
                        watercut_for_sensor = model->cap_mixture_model_030_040(raw_norm_cap);
                    }
                    else{
                        if(raw_norm_cap<=15001){
                            watercut_for_sensor = model->cap_mixture_model_000_030(raw_norm_cap);
                        }
                        else{
                            watercut_for_sensor=0;
                        }
                    }
                }
            }
        }
        if(watercut_for_sensor>100){
            watercut_for_sensor = 100;
            vector_watercut_cap_laminar_ei[i] = watercut_for_sensor;
        }else{
            vector_watercut_cap_laminar_ei[i] = watercut_for_sensor;
        }
        //qDebug()<<"wc capacitivo "<<watercut_for_sensor;
    }
}

void jptwatercut::calcule_model_laminar_ei(){
    cap_laminar_ei = model->cap_laminar(vector_watercut_cap_laminar_ei);

    //qDebug()<<"res laminar EI "<<res_laminar_EI;
    qDebug()<<"cap laminar EI "<<cap_laminar_ei;
}

void jptwatercut::regimen_laminar_ei(){
    //AGREGAR VECTORES A MODELO INVERSO
    float factor =0;

    factor = float(ui->spin_factor->value());

    int condi =ui->spin_value_th_oil_lei->value();
    int diff =ui->spin_value_th_diff_lei->value();

    float counter_true=0;

    //Average resistivo

    for (int i=0;i<6;i++) {
        if(vector_normalize_res[i]>condi && on_off_inverse==1){
            if(vector_normalize_cap[i]<vector_normalize_res[i] && (vector_normalize_res[i]-vector_normalize_cap[i])>diff){

                //list_val_resis.append(true);
                //list_val_cap.append(true);

                //resis * factor
                vector_factor_res_laminar_ei[i] = vector_normalize_res[i] * ui->spin_factor->value();

                //cap * factor
                vector_factor_cap_laminar_ei[i] = vector_normalize_cap[i] * ui->spin_factor->value();

                //sum_true+=0.16667;

            }else{
                //resis * factor
                vector_factor_res_laminar_ei[i] = 0;

                //cap * factor
                vector_factor_cap_laminar_ei[i] = 0;


                //list_val_resis.append(false);
                //list_val_cap.append(false);
            }
        }else{
            //resis * factor
            vector_factor_res_laminar_ei[i] = 0;

            //cap * factor
            vector_factor_cap_laminar_ei[i] = 0;

            //list_val_resis.append(false);
            //list_val_cap.append(false);
        }
    }

    /*
    for (int g=0;g<min;g++) {
        for (int i=0;i<6;i++) {
            if(val_resis[g][i] == true){
                // qDebug()<<"Sensor Resis "<<i+1<<" LEI ";
            }
            if(val_cap[g][i] == true){
                // qDebug()<<"Sensor Capa "<<i+1<<" LEI ";
            }
        }
    }

    */
    watercut_cap_laminar_ei();
    calcule_model_laminar_ei();
}

void jptwatercut::watercut_res_turbulent(){
    int resis_water = vector_values_calibration[2];
    int resis_oil= vector_values_calibration[0];
    int total_counts_res=0;

    //Pendiente
    float m = (-100)/float((resis_oil-resis_water));
    //Vector Y0
    float Y0 = float((-m*resis_water)+100);

    for (int i=0;i<6;i++) {
        total_counts_res = vector_normalize_res[i];

        float watercut_resis = float((m*total_counts_res)+Y0);

        if(watercut_resis>=100){
            watercut_resis = 100;
        }else if(watercut_resis<=0){
            watercut_resis = 0;
        }
        vector_watercut_res_turbulent[i] = watercut_resis;
    }
}

void jptwatercut::watercut_cap_turbulent(){
    //las funciones estaticas estan definidas en JPTModel como cap_mixture y d estas se tomaran la funcion de 0a 30,
    //la funcion de 30 a 40 y de 40 a70%
    //las funciones siempre necesitan una valor de capacitancia para retornar el valor del porcentaje de agua equivalente.
    //Ahora bien, primero obtenemos el valor para el calculo de la grafica. Ccwater y CcformationWater

    int capa_water = vector_values_calibration[6];
    int capa_water_f = vector_values_calibration[7];
    //con estos valores debo de generar mi curva dinamica.
    /*
         * NOTA: Para generar la curva se necesitan dos puntos, en este caso serian (100,CFw) y (X,Cw)
         * el valor de X en mi punto P2 esta dado por el porcentaje de agua que existe en ese punto,
         * para ello se evalue la funcion cap_mixture_40_70 del jptmodel en CW y con eso obtenems el valor de x.
         *
         */
    float percent_p2 = float(model->cap_mixture_model_040_070(capa_water));
    //la siguiente e la funcion dinamica que toma el valor de X[i] y calcula los valores.
    //funcion=((-1)*(percent_p2-100)/(CWater-CF_Water))*(CWater-X[i])+percent_p2;


    for (int i=0;i<6;i++) {
        int raw_norm_cap=0;

        raw_norm_cap = vector_normalize_cap[i];

        float watercut_for_sensor = ((-1)*(percent_p2-100)/(capa_water-capa_water_f))*(capa_water-capa_water_f)+percent_p2;


        if(raw_norm_cap<=capa_water_f){
            //todo antes de las formaciones es agua. entonces f(x) seria 100% agua.
            raw_norm_cap=100;
        }
        else{
            if(raw_norm_cap<=capa_water){
                // no haga nada es el valor actual de wc_per_sensor con la curva dinamica
            }
            else{
                if(raw_norm_cap<=7501){
                    watercut_for_sensor = model->cap_mixture_model_040_070(raw_norm_cap);
                }
                else{
                    if(raw_norm_cap<=13801){
                        watercut_for_sensor = model->cap_mixture_model_030_040(raw_norm_cap);
                    }
                    else{
                        if(raw_norm_cap<=15001){
                            watercut_for_sensor = model->cap_mixture_model_000_030(raw_norm_cap);
                        }
                        else{
                            watercut_for_sensor=0;
                        }
                    }
                }
            }
        }
        if(watercut_for_sensor>100){
            watercut_for_sensor = 100;
            vector_watercut_cap_turbulent[i] = watercut_for_sensor;
        }else{
            vector_watercut_cap_turbulent[i] = watercut_for_sensor;
        }
        //qDebug()<<"wc capacitivo "<<watercut_for_sensor;
    }
}

int jptwatercut::subs_values(int value1, int value2){
    int subs = 0;
    subs  = abs(value1 - value2);
    return subs;
}

void jptwatercut::calcule_model_turbulent(){
    res_turbulent = model->res_turbulent(vector_watercut_res_turbulent);

    cap_turbulent = model->cap_turbulent(vector_watercut_cap_turbulent);

    qDebug()<<"res turbulent "<<res_turbulent;
    qDebug()<<"cap turbulent "<<cap_turbulent;
}

void jptwatercut::regimen_turbulent(){
    int spin_flow = int(ui->spin_value_cps_tur->value());

    //este es primero para evaluar los valores resistivos
    for (int i=0;i<6;i++) {
        qDebug()<<"value lista res "<<vector_normalize_res[i];
        for (int j = 0; j < 6; ++j) {
            if(i!=j){
                int value=0;
                value = subs_values(vector_normalize_res[i], vector_normalize_res[j]);
                if(value <= spin_flow){
                    //list_bool_res.append(true);
                    qDebug()<<"value "<<value<<i+1<<" "<<j+1;
                }else{
                    //list_bool_res.append(false);
                }
            }
        }
    }

    for (int i=0;i<6;i++) {
        //qDebug()<<"value lista cap "<<list_cap[i];
        for (int j = 0; j < 6; ++j) {
            if(i!=j){
                int value=0;
                value = subs_values(vector_normalize_cap[i], vector_normalize_cap[j]);
                if(value <= spin_flow){
                    //list_bool_cap.append(true);
                    qDebug()<<"value "<<value <<i+1<<" "<<j+1;
                }else{
                    //list_bool_cap.append(false);
                }
            }
        }
    }
    /*
    for (int h=0;h<list_bool_cap.size();h++) {
        if(list_bool_res[h] && list_bool_cap[h]){
            counter_true++;
        }
    }*/
    /*
    if(counter_cap == min && counter_res == min){
    }
    */

    watercut_res_turbulent();
    watercut_cap_turbulent();
    calcule_model_turbulent();
}

void jptwatercut::watercut_res_turbulent_lam(){
    int resis_water = vector_values_calibration[2];
    int resis_oil= vector_values_calibration[0];
    int total_counts_res=0;

    //Pendiente
    float m = (-100)/float((resis_oil-resis_water));
    //Vector Y0
    float Y0 = float((-m*resis_water)+100);

    for (int i=0;i<6;i++) {
        total_counts_res = vector_normalize_res[i];

        float watercut_resis = float((m*total_counts_res)+Y0);
        if(watercut_resis>=100){
            watercut_resis = 100;
        }else if(watercut_resis<=0){
            watercut_resis = 0;
        }
        vector_watercut_res_turbulent_lam[i] = watercut_resis;
    }
}

void jptwatercut::watercut_cap_turbulent_lam(){
    //las funciones estaticas estan definidas en JPTModel como cap_mixture y d estas se tomaran la funcion de 0a 30,
    //la funcion de 30 a 40 y de 40 a70%
    //las funciones siempre necesitan una valor de capacitancia para retornar el valor del porcentaje de agua equivalente.
    //Ahora bien, primero obtenemos el valor para el calculo de la grafica. Ccwater y CcformationWater

    int capa_water = vector_values_calibration[6];
    int capa_water_f = vector_values_calibration[7];
    //con estos valores debo de generar mi curva dinamica.
    /*
         * NOTA: Para generar la curva se necesitan dos puntos, en este caso serian (100,CFw) y (X,Cw)
         * el valor de X en mi punto P2 esta dado por el porcentaje de agua que existe en ese punto,
         * para ello se evalue la funcion cap_mixture_40_70 del jptmodel en CW y con eso obtenems el valor de x.
         *
         */
    float percent_p2 = float(model->cap_mixture_model_040_070(capa_water));
    //la siguiente e la funcion dinamica que toma el valor de X[i] y calcula los valores.
    //funcion=((-1)*(percent_p2-100)/(CWater-CF_Water))*(CWater-X[i])+percent_p2;


    for (int i=0;i<6;i++) {
        int raw_norm_cap=0;

        raw_norm_cap = vector_normalize_cap[i];

        float watercut_for_sensor = ((-1)*(percent_p2-100)/(capa_water-capa_water_f))*(capa_water-capa_water_f)+percent_p2;


        if(raw_norm_cap<=capa_water_f){
            //todo antes de las formaciones es agua. entonces f(x) seria 100% agua.
            raw_norm_cap=100;
        }
        else{
            if(raw_norm_cap<=capa_water){
                // no haga nada es el valor actual de wc_per_sensor con la curva dinamica
            }
            else{
                if(raw_norm_cap<=7501){
                    watercut_for_sensor = model->cap_mixture_model_040_070(raw_norm_cap);
                }
                else{
                    if(raw_norm_cap<=13801){
                        watercut_for_sensor = model->cap_mixture_model_030_040(raw_norm_cap);
                    }
                    else{
                        if(raw_norm_cap<=15001){
                            watercut_for_sensor = model->cap_mixture_model_000_030(raw_norm_cap);
                        }
                        else{
                            watercut_for_sensor=0;
                        }
                    }
                }
            }
        }
        if(watercut_for_sensor>100){
            watercut_for_sensor = 100;
            vector_watercut_cap_turbulent_lam[i] = watercut_for_sensor;
        }else{
            vector_watercut_cap_turbulent_lam[i] = watercut_for_sensor;
        }
        //qDebug()<<"wc capacitivo "<<watercut_for_sensor;
    }
}

void jptwatercut::calcule_model_turbulent_lam(){
    res_lam_turbulent = model->res_turbulent_laminar(vector_watercut_res_turbulent_lam);

    cap_lam_turbulent = model->cap_turbulent_laminar(vector_watercut_cap_turbulent_lam);

    qDebug()<<"res lam turbulent "<<res_lam_turbulent;
    qDebug()<<"cap lam turbulent "<<cap_lam_turbulent;

}

void jptwatercut::regimen_turbulent_lam(){
    int spin_flow = int(ui->spin_value_cps_tur->value());

    if(vector_normalize_res[0]<ui->spin_value_th_water_lam->value() && vector_normalize_res[5]>ui->spin_value_th_oil_lam->value() &&
            vector_normalize_cap[0]<ui->spin_value_th_water_lam->value() && vector_normalize_cap[5]>ui->spin_value_th_oil_lam->value()){

        //este es primero para evaluar los valores resistivos
        for (int i=0;i<6;i++) {
            qDebug()<<"value lista res "<<vector_normalize_res[i];
            for (int j = 0; j < 6; ++j) {
                if(i!=j){
                    if(i!=0 && i!=3 && i!=j){
                        if(j!=0 && j!=3){
                            int value=0;
                            value = subs_values(vector_normalize_res[i], vector_normalize_res[j]);
                            if(value <= spin_flow){
                                //list_bool_res.append(true);
                                qDebug()<<"value "<<value<<i+1<<" "<<j+1;
                            }else{
                                //list_bool_res.append(false);
                            }
                        }
                    }
                }
            }
        }

        for (int i=0;i<6;i++) {
            //qDebug()<<"value lista cap "<<list_cap[i];
            for (int j = 0; j < 6; ++j) {
                if(i!=j){
                    int value=0;
                    value = subs_values(vector_normalize_cap[i], vector_normalize_cap[j]);
                    if(value <= spin_flow){
                        //list_bool_cap.append(true);
                        qDebug()<<"value "<<value <<i+1<<" "<<j+1;
                    }else{
                        //list_bool_cap.append(false);
                    }
                }
            }
        }
    }else{
        qDebug()<<"No cumple para laminar";
    }

    /*
    for (int h=0;h<list_bool_cap.size();h++) {
        if(list_bool_res[h]){
            counter_res++;
        }
        if(list_bool_cap[h]){
            counter_cap++;
        }
        if(!list_bool_res[h] || !list_bool_cap[h]){
            break;
        }
    }

    if(counter_cap == min && counter_res == min){
        watercut_capacitive_turb();
        watercut_resistivity_turb();
    }
    */
    watercut_res_turbulent_lam();
    watercut_cap_turbulent_lam();
    calcule_model_turbulent_lam();
}

void jptwatercut::fill_data_auto(){

    QList<int> data_res;
    QList<int> data_cap;

    data_res.clear();
    data_cap.clear();

    for (int i=0;i<6;i++) {
        data_res.append(vector_normalize_res[i]);
        data_cap.append(vector_normalize_cap[i]);
    }

    vector_smart_data_res.append(data_res);
    vector_smart_data_cap.append(data_cap);

    counter_samples++;
    emit send_message_console_smart("Sample taken #"+QString::number(counter_samples));
}

void jptwatercut::smart_model_id(){
    int condi = ui->spin_value_th_oil_lei->value(), diff =ui->spin_value_th_diff_lei->value();

    int spin_flow = int(ui->spin_value_cps_tur->value());

    int spin_takes = int(ui->spin_entries_auto->value());

    QList<int> list_cap, list_res;

    float counter_true_lam =0, counter_true_lei=0, counter_true_turb=0, counter_true_turb_lam=0;

    for (int j=0;j<spin_takes;j++) {
        int temp_resis1 =0, temp_resis2 =0, temp_resis3 =0, temp_resis4 =0, temp_resis5 =0,
                temp_resis6 =0, temp_capa1=0, temp_capa2 =0, temp_capa3 =0, temp_capa4 =0,
                temp_capa5 =0, temp_capa6=0;

        list_cap.clear();
        list_res.clear();

        temp_resis1 = vector_smart_data_res[j][0];
        temp_capa1 = vector_smart_data_cap[j][0];

        temp_resis2 = vector_smart_data_res[j][1];
        temp_capa2 = vector_smart_data_cap[j][1];

        temp_resis3 = vector_smart_data_res[j][2];
        temp_capa3 = vector_smart_data_cap[j][2];

        temp_resis4 = vector_smart_data_res[j][3];
        temp_capa4 = vector_smart_data_cap[j][3];

        temp_resis5 = vector_smart_data_res[j][4];
        temp_capa5 = vector_smart_data_cap[j][4];

        temp_resis6 = vector_smart_data_res[j][5];
        temp_capa6 = vector_smart_data_cap[j][5];

        //EVALUACION PARA LAMINAR
        if(temp_resis1<ui->spin_value_th_water_lam->value() && temp_resis4>ui->spin_value_th_oil_lam->value() &&
                temp_capa1<ui->spin_value_th_water_lam->value() && temp_capa4>ui->spin_value_th_oil_lam->value()){
            //qDebug()<<"TRUE LAMINAR";
            counter_true_lam++;
        }

        //EVALUACION PARA LAMINAR EI
        if(temp_resis1>condi){
            if(temp_capa1<temp_resis1 && (temp_resis1-temp_capa1)>diff){
                counter_true_lei+=0.16667;
            }
        }

        if(temp_resis2>condi){
            if(temp_capa2<temp_resis2 && (temp_resis2-temp_capa2)>diff){
                counter_true_lei+=0.16667;
            }
        }

        if(temp_resis3>condi){
            if(temp_capa3<temp_resis3 && (temp_resis3-temp_capa3)>diff){
                counter_true_lei+=0.16667;
            }
        }

        if(temp_resis4>condi){
            if(temp_capa4<temp_resis4 && (temp_resis4-temp_capa4)>diff){
                counter_true_lei+=0.16667;
            }
        }

        if(temp_resis5>condi){
            if(temp_capa5<temp_resis5 && (temp_resis5-temp_capa5)>diff){
                counter_true_lei+=0.16667;
            }
        }

        if(temp_resis6>condi){
            if(temp_capa6<temp_resis6 && (temp_resis6-temp_capa6)>diff){
                counter_true_lei+=0.16667;
            }
        }

        //se llenan las listas resistivas
        list_res.append(temp_resis1);
        list_res.append(temp_resis2);
        list_res.append(temp_resis3);
        list_res.append(temp_resis4);
        list_res.append(temp_resis5);
        list_res.append(temp_resis6);
        //se llenan las listas capacitivas
        list_cap.append(temp_capa1);
        list_cap.append(temp_capa2);
        list_cap.append(temp_capa3);
        list_cap.append(temp_capa4);
        list_cap.append(temp_capa5);
        list_cap.append(temp_capa6);

        //EVALUACION PARA TURBULENTO

        //se hace el contador con 1/60 ya que son 30 evaluaciones de resistivos y 30 capacitivos

        for (int i=0;i<6;i++) {
            for (int j = 0; j < 6; ++j) {
                if(i!=j){
                    int value=0;
                    value = subs_values(list_res[i], list_res[j]);
                    if(value <= spin_flow){
                        counter_true_turb+=0.01666;
                    }
                }
            }
        }

        for (int i=0;i<6;i++) {
            for (int j = 0; j < 6; ++j) {
                if(i!=j){
                    int value=0;
                    value = subs_values(list_cap[i], list_cap[j]);
                    if(value <= spin_flow){
                        counter_true_turb+=0.01666;
                    }
                }
            }
        }

        //EVALUACION PARA TURBULENTO LAMINAR

        if(temp_resis1<ui->spin_value_th_water_lam->value() && temp_resis4>ui->spin_value_th_oil_lam->value() &&
                temp_capa1<ui->spin_value_th_water_lam->value() && temp_capa4>ui->spin_value_th_oil_lam->value()){

            for (int i=0;i<6;i++) {
                //qDebug()<<"value lista res "<<list_res[i];
                for (int j=0;j<6;++j) {
                    if(i!=0 && i!=3 && i!=j){
                        if(j!=0 && j!=3){
                            int value=0;
                            value = subs_values(list_res[i], list_res[j]);
                            if(value <= spin_flow){
                                counter_true_turb_lam+=0.025;
                            }
                        }
                    }
                }
            }

            for (int i=0;i<6;i++) {
                //qDebug()<<"value lista cap "<<list_cap[i];
                for (int j=0;j<6;++j) {
                    if(i!=0 && i!=3 && i!=j){
                        if(j!=0 && j!=3){
                            int value=0;
                            value = subs_values(list_cap[i], list_cap[j]);
                            if(value <= spin_flow){
                                counter_true_turb_lam+=0.025;
                            }
                        }
                    }
                }
            }

        }
    }

    //calculo para porcentaje de cumplimiento de modelo

    //LAMINAR
    float percent_true_lam = (counter_true_lam * 100)/spin_takes;

    qDebug()<<"% LM"<<percent_true_lam;

    ui->lcd_id_laminar->display(QString::number(percent_true_lam,'f',2) + "%");

    //LAMINAR EI
    float percent_true_lei = (counter_true_lei*100)/spin_takes;

    qDebug()<<"% LEI "<<percent_true_lei;

    ui->lcd_id_laminar_ei->display(QString::number(percent_true_lei, 'f',2) + "%");

    //TURBULENTO
    float percent_true_turb = (counter_true_turb*100)/spin_takes;

    ui->lcd_id_turbulent->display(QString::number(percent_true_turb,'f',2) + "%");
    qDebug()<<"% TURB "<<percent_true_turb;

    //TURBULENTO LAM
    float percent_true_turb_lam = (counter_true_turb_lam*100)/spin_takes;
    //float percent_true_turb_ = (counter_true_turb_lam*100)/min;

    ui->lcd_id_turbulent_lam->display(QString::number(percent_true_turb_lam,'f',2) + "%");
    //ui->line_percent_turb_lam_2->setText(QString::number(percent_true_turb_,'f',2) + "%");

    // qDebug()<<"% LAM "<<percent_true_turb_lam;
    qDebug()<<"% TURB LAM "<<percent_true_turb_lam;

    smart_mode = true;
}

void jptwatercut::execute_regimen(){
    if(laminar){
        regimen_laminar();
    }else if(lei){
        regimen_laminar_ei();
    }else if(turbulent){
        regimen_turbulent();
    }else if(turb_lam){
        regimen_turbulent_lam();
    }else{

    }
}

/*!
    * Este metodo recibe toda la parte de las tramas entrantes enviadas por el JPTMONITORING,
    * primero que nada llena en la interfaz inicial los valores que llegan,
    * seguido de esto se muestra el valor de temperatura del equipo,
    * despues entra en el metodo que calcula la normalizacion de la data esto va de acuerdo a la necesidad,
    * y por ultimo calcula la salinidad. Valores iniciales que entrega el sistema.
    *
    * Luego de esto comienza la verificacion del regimen en el que se encuentre ya que tenemos dos formas de saberlo,
    * la primera es de manera automatica de acuerdo a la cantidad de muestras que se tomen el sistema determina que tipo es,
    * y la opcion es la que pertenece al hecho de escoger el modelo de manera manual forzando al sistema
    * a que debe de hacer ese calculo y no mas.
*/

void jptwatercut::receive_json_from_tcpclient(QStringList _raw,QStringList _eng, QStringList _sensors){
    fill_lcd_raw(_raw);
    //Show eng temperature
    ui->lcd_temp_eng->display(_eng[12]);
    //Normalize data
    normalize_raw(_raw);

    salinity();

    if(smart_mode){
        execute_regimen();
        qDebug("auto");
    }else{
        if(auto_mode_id){
            if((counter_samples) == ui->spin_entries_auto->value()){
                smart_model_id();
            }else{
                fill_data_auto();
            }
        }else if(manual_mode_id){
            execute_regimen();
            qDebug("manual");
        }
    }


    for (int i=0;i<6;i++) {
        //qDebug()<<"res "<< vector_normalize_res[i];
        //qDebug()<<"cap "<< vector_normalize_cap[i];
    }

}
